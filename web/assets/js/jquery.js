$(document).ajaxStart(function(){
    $("#wait").css("display", "block");
});
$(document).ajaxComplete(function(){
    $("#wait").css("display", "none");
});
$(document).ready(function(){
	$("#show-result").click(function(){
			var id=$("#id_num").val();
			var id_booking = $("#id_booking").val();
			console.log(id_booking);
			if(id==''){
				$(".error_log").html('Please enter your ID');
			}
			else if(id_booking==''){
				$(".error_log").text("Please enter your Booking ID");
			}
			else{
				$.ajax({
				  type: 'POST',
			      url: 'ajax/ajax_load_result.php',
			      data: {
			         uid: id,
			         bid: id_booking
			      },
			      success: function(data) {
			      	if(data==1) $(".isa_error").css("display","block");
			        else $("#show-result-block").html(data);
			         
			      }
			   });
			}
			
		});
	$(".content-box").hover(function(){
		$(this).find("span").css("background","#ff5454");
	});
	$(".content-box").mouseleave(function(){
		$(this).find("span").css("background","#3b3b3b");

	});
	$("#have-id").click(function(){
		$.ajax({
		type: 'POST',
		url: './ajax/ajax_have_id.php',
		
		success: function(data){
			$("#booking-box").html(data);
		}
		});
	});

	$("#have-no-id").click(function(){
		$.ajax({
		type: 'POST',
		url: './ajax/ajax_have_no_id.php',
		
		success: function(data){
			$("#booking-box").html(data);
		}
		});
	});
	$("#id-booking-next").click(function(){
		var id=$("#id-number").val();
		$.ajax({
			type: 'POST',
			url: './ajax/ajax_booking_id.php',
			
			success: function(data){
				$("#booking-box").html(data);
			}
			});
	});
	
	
})
function noID(){
		$.ajax({
		type: 'POST',
		url: './ajax/ajax_have_no_id.php',
		
		success: function(data){
			$("#booking-box").html(data);
		}
		});
	}
function forgotEmail(){
	var mail=$("#email-verify").val();
	if(mail==""){
		$("#fillout").css("display","block");
	}
	else{
		$.ajax({
			type: "POST",
            url: "./ajax/ajax_forgot_id.php",
            data: {mail:mail},
            success: function(data)
            {
            	//alert(data);
            	if(data==1){
            		$("#fillout").css("display","none");
					$("#send-success").css("display","block");
            	}else {
            		$("#fillout span").html("Incorrect indentity number or your account does not exist.");
            		$("#fillout").css("display","block");
            	}
            }
		});
		
	}
}
function validateFormBooking(){
	var name=$("#speed");
	if(name.val()==0){
		$("#booking-error").css("display","block");
		//name.select();
		return false;
	}
	name=$("#speed1");
	if(name.val()==0){
		$("#booking-error").css("display","block");
		//name.select();
		return false;
	}
	name=$("#speed2");
	if(name.val()==0){
		$("#booking-error").css("display","block");
		//name.select();
		return false;
	}
	return true;
}
function validateFormUser()
{
	var name=$("#name");
	if(name.val()==""){
		$("#user-error").css("display","block");
		name.select();
		return false;
	}
	name=$("#idnum");
	if(name.val()==""){
		$("#user-error").css("display","block");
		name.select();
		return false;
	}
	name=$("#datepicker");
	if(name.val()==""){
		$("#user-error").css("display","block");
		name.select();
		return false;
	}
	name=$("#address");
	if(name.val()==""){
		$("#user-error").css("display","block");
		name.select();
		return false;
	}
	name=$("#phone");
	if(name.val()==""){
		$("#user-error").css("display","block");
		name.select();
		return false;
	}
	name=$(".mau");
	if(name.attr("src")==""){
		$("#user-error").css("display","block");
		name.select();
		return false;
	}
	
	return true;
}
function uploadFile(id,code,checked){
    var formData = new FormData();
    var file=$('#img-iden')[0].files[0];
	formData.append('file', file);
	formData.append('code', code);
    $.ajax({
            type: "POST",
            url: "./ajax/ajax_upload_file.php",
            data: formData,
            processData: false,  // tell jQuery not to process the data
       		contentType: false,  // tell jQuery not to set contentType
            success: function(data)
	           {
	               //alert("Data: "+data); // show response from the php script.
	               if(data==1 && checked==true) confirm_info(code);
	               else if (data==1 && checked==false) {
	               	//alert(id);
	               	window.location="/booking-detail-"+code+"-"+id;
	               	//show_booking(id,code);
	               }
	               else {
	               	$("#user-error span").html("Sorry, your image is too large");
               		$("#user-error").css("display","block");

	               }
	           }
         });
	    
}
function checkout(code){
	//alert(code);
	$("#booking-error").css("display","none");
	$("#user-error").css("display","none");
	if(validateFormUser()&&validateFormBooking())
	{
		var fd = new FormData();
		var other_data = $('#register-form').serializeArray();
	    $.each(other_data,function(key,input){
	        fd.append(input.name,input.value);
	    });
	    fd.append("code",code);
		$.ajax({
	           type: "POST",
	           url: "./ajax/ajax_checkout.php",
	           data:fd,
	           processData: false,  // tell jQuery not to process the data
	       		contentType: false,  // tell jQuery not to set contentType
	            
	           success: function(data)
	           {
	           	   //alert(data);
		           	if(data==1){
		           		$("#user-error span").html("Sorry, this email or identity number was exists. <a  data-toggle='modal' data-target='#myModal'>Forgot your ID?</a>");
		               	$("#user-error").css("display","block");
			         }
	                else if(data==2 || data==3){
	                	$("#user-error span").html("Sorry, something went wrong.");
	               		$("#user-error").css("display","block");
		            }
		           	else uploadFile(data,code,false);
		        }
         });
	}
	
}
function confirm_info(code)
{
	var id_place = $('#speed2').val();
	//alert(id_place);
	$.ajax({
            type: "POST",
            url: "./ajax/ajax_confirm_info.php",
            data: {code:code,detail:id_place},
            success: function(data)
	           {
	               //alert(data); // show response from the php script.
	           		$("#main-box").html(data);
	           }
         });
}


function register(){
	$("#booking-error").css("display","none");
	$("#user-error").css("display","none");
	
	if(validateFormUser()&&validateFormBooking())
	{
		var file=$('#img-iden')[0].files[0];
		if(file.size>=2097152){
			$("#user-error span").html("Sorry, your image is too large");
	   		$("#user-error").css("display","block");
	   		return false;
		}
		$.ajax({
	           type: "POST",
	           url: "./ajax/ajax_register.php",
	           data: $("#register-form").serialize(), // serializes the form's elements.
	           //data:formData,
	           success: function(data)
	           {
	               if(data=="false") {
	               		$("#user-error span").html("Sorry, something went wrong.");
	               		$("#user-error").css("display","block");
	               }
	               else if( data=="exist"){
	               	$("#user-error span").html("Sorry,this email or identity number was exists. <a  data-toggle='modal' data-target='#myModal'>Forgot your ID?</a>");
	               	$("#user-error").css("display","block");
	               }
	           	   else{uploadFile(1,data,true);}
	           }
	         });
	}
}

function contact_us(){
	var sub_name = $('#subcribe_name').val();
	var sub_email = $('#subcribe_email').val();
	var sub_cont = $('#subcribe_cont').val();

	console.log('cont', sub_cont);

	if(sub_email === ''){
		$('.alert-danger').slideDown(300);
		$('.alert-danger').append('Vui lòng điền đầy đủ thông tin');
		return false;
	}

	if(sub_cont === ''){
		$('.alert-danger').slideDown(300);
		$('.alert-danger').append('Vui lòng điền đầy đủ thông tin');
		return false;
	}


	$.ajax({
		type: 'POST',
		url: './ajax/ajax_contact.php',
		data: {
			SUB_NAME : sub_name,
			SUB_EMAIL : sub_email,
			SUB_CONT : sub_cont
		},

		success: function(data){
			console.log('success');
			$('.alert').removeClass('alert-danger').addClass('alert-success');
			$('.alert').empty().append('Cảm ơn bạn đã liên hệ với chúng tôi.');
			$('.alert').slideDown(300);
			
			/*$('.contact-form').slideUp();*/
		},
		error: function(data){
			console.log('error', data);
		}
	});

}