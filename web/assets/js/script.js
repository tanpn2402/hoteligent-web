// function onBlockMouseOut() {
// 	//console.log("Mouse out", element)
// 	$(this).find('.block-description').animate({bottom: '0px'})
// }

// function onBlockMouseOver() {
// 	//console.log("Mouse enter", element.classList)

// 	$(this).find('.block-description').animate({top: '0px'})
// }


const FORM_LOGIN_PAGE = 0
const FORM_REGISTER_PAGE = 1
const FORM_FORGET_PAGE = 1


function toggleSearch() {
	$("#app").toggleClass("app-search");
	$("#search").slideToggle(150);
	$('.navbar').toggleClass('fool');


	if( !$("#app").hasClass("app-search") ) {
		$("body").css({"overflow":"auto"});
		
	} else {
		$("body").css({"overflow":"hidden", "width":"100%", "height":"100%"});
	}
	// $(document).keypress(function(e) {
	//     if(e.which == 13) {
	//         alert('You pressed enter!');
	//     }
	// });
}
function toggleFormLogin() {
	$("#login").fadeToggle()
	$("#app").toggleClass("app-login");
	if( !$("#app").hasClass("app-login") ) {
		$("body").css({"overflow":"auto"});
		
	} else {
		$("body").css({"overflow":"hidden", "width":"100%", "height":"100%"});
	}
}
function toggleSideMenu() {
	$("#side-menu").toggleClass("show");
}
$(document).ready(function(){

	$('.slick-slider').slick({
		autoplay: true,
  		autoplaySpeed: 50000,
  		arrows: false,
  		dots: true
	});

	$('.ad-box-slider').slick({
		autoplay: true,
  		autoplaySpeed: 3000,
  		arrows: false,
  		dots: true
	});

	$('.form-wrapper').slick({
		autoplay: false,
  		arrows: false,
  		dots: false,
  		draggable: false
	});

	$('.slick-scroll').slick({
  		arrows: true,
  		dots: true,
  		infinite: true,
	    slidesToShow: 5,
	    slidesToScroll: 5,
		responsive: [
			{
			  breakpoint: 1024,
			  settings: {
			    slidesToShow: 5,
			    slidesToScroll: 5,
			    infinite: true,
			    dots: true
			  }
			},
			{
			  breakpoint: 600,
			  settings: {
			    slidesToShow: 3,
			    slidesToScroll: 3
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
			    slidesToShow: 2,
			    slidesToScroll: 2
			  }
			}
  		]
	});


	$(".search-trigger").click(() => {
		toggleSearch()
	})

	$(".btn-respnsive").click(() => {
		toggleSideMenu()
	})

	$(".form-slide-trigger").click(function() {
		console.log($(this).attr("value"))
		$(".form-wrapper").slick("slickGoTo", $(this).attr("value"))
	})
	

	$(".form-trigger").click(function() {
		toggleFormLogin()
	})
	$(".login-trigger").click(function() {
		$(".form-wrapper").slick("slickGoTo", 0)
		$(".ad-box-slider").slick("slickGoTo", 0)
		toggleFormLogin()
	})
	$(".register-trigger").click(function() {
		$(".form-wrapper").slick("slickGoTo", 1)
		$(".ad-box-slider").slick("slickGoTo", 0)
		toggleFormLogin()
	})

	$(".block").click(function() {
		console.log('asd')
		window.location = "./singlepage.html"
	})


	/// magazine page
	$("#maxis-trigger").click(function() {
		$("#content-list").toggleClass("hide")
		// var height = $("#content-list").height()
		// console.log(height)
		// $("#maxis-trigger").css("transform", "translateY(0px)")
	})

	$("#minus-trigger").click(function() {
		$("#content-list").toggleClass("hide")
		// var height = $("#content-list").height()
		// console.log(height)
		// $("#maxis-trigger").css("transform", "translateY(" + (height/2 -50 ) +  "px)")
	})

	/*Multi rows slider*/
	$('.multi').slick({
	 rows: 2,
	 autoplay: 1,
	 slidesToShow: 3,
	 responsive: [
	     {
	     	breakpoint: 768,
	     	settings: {
	        	slidesToShow: 1           
	     	}
	    }
	  ]            
	}); 
	
	$('.slick-newest').slick({
	 rows: 1,
	 autoplay:0,
	 autoplaySpeed: 3000,
	 slidesToShow: 4,
	 responsive: [
	     {
	     	breakpoint: 768,
	     	settings: {
	        	slidesToShow: 1           
	     	}
	    }
	  ]            
	}); 
	$('.ads-slick').slick({
	 rows: 1,
	 autoplay:1,
	 slidesToShow: 1,
	 responsive: [
	     {
	     	breakpoint: 768,
	     	settings: {
	        	slidesToShow: 1           
	     	}
	    }
	  ]            
	}); 
	$('.page-item').hover(function(){
		$(this).find('a').addClass('pagi-hover');
	});
	$('.page-item').mouseleave(function(){
		$(this).find('a').removeClass('pagi-hover');
	});
	$('.place').hover(function(){
		$(this).find('p').addClass('place-hover');
	});
	$('.place').mouseleave(function(){
		$(this).find('p').removeClass('place-hover');
	});

	$('#tab-slide-0').slick({
		rows: 1,
		autoplay:1,
		slidesToShow: 4,
		autoplaySpeed: 3000,
		responsive: [
			{
				breakpoint: 820,
				settings: {
				   slidesToShow: 3         
				}
		   },
			{
				breakpoint: 768,
				settings: {
				   slidesToShow: 1           
				}
		   }
		 ]            
	   }); 
	
	   $('#tab-slide-1').slick({
		rows: 1,
		autoplay:1,
		slidesToShow: 4,
		autoplaySpeed: 3000,
		responsive: [
			{
				breakpoint: 820,
				settings: {
				   slidesToShow: 3         
				}
		   },
			{
				breakpoint: 768,
				settings: {
				   slidesToShow: 1           
				}
		   }
		 ]            
	   }); 
	 
	// $('.animated').hover(function(){
		
	// 	$(this).find('.block-description').animate({top: '0px'});
	// 	//$(this).find('.block-description').toggleClass('top');
	// })

	// $('.animated').mouseleave(function(){
	// 	$(this).find('.block-description').addClass('top');
	// 	$(this).find('.block-description').animate({bottom: '0px'});
	// });

	// $("#owl-example").owlCarousel({
	// 	navigation : true, 
	//   	slideSpeed : 300,
	//   	paginationSpeed : 400,
	//   	singleItem: true,
	// 	pagination: false,
	// 	rewindSpeed: 500
	// });
});
// $.global = new Object();

// $.global.item = 1;
// $.global.total = 0;

// $(document).ready(function() 
// 	{
	
// 	var WindowWidth = $(window).width();
// 	var SlideCount = $('#slides li').length;
// 	var SlidesWidth = SlideCount * WindowWidth;
	
//    $.global.item = 0;
//     $.global.total = SlideCount; 
    
// 	$('.slide').css('width',WindowWidth+'px');
// 	$('#slides').css('width',SlidesWidth+'px');

//    $("#slides li:nth-child(1)").addClass('alive');
    
//   $('#left').click(function() { Slide('back'); }); 
//   $('#right').click(function() { Slide('forward'); }); 
        
//   });

// function Slide(direction)
// 	{
   
//     if (direction == 'back') { var $target = $.global.item - 1; }
//     if (direction == 'forward') { var $target = $.global.item + 1; }  
    
//     if ($target == -1) { DoIt($.global.total-1); } 
//     else if ($target == $.global.total) { DoIt(0); }  
//     else { DoIt($target); }
    
    
// 	}

// function DoIt(target)
//   {
   
//     var $windowwidth = $(window).width();
// 	var $margin = $windowwidth * target; 
//     var $actualtarget = target+1;
    
//     $("#slides li:nth-child("+$actualtarget+")").addClass('alive');
    
//     $('#slides').css('transform','translate3d(-'+$margin+'px,0px,0px)');	
    
//     $.global.item = target; 
    
//   $('#count').html($.global.item+1);
    
//   }

//Han's code

	/*Slider homepage*/
		jssor_1_slider_init = function() {

            var jssor_1_SlideshowTransitions = [
              {$Duration:800,x:0.3,$During:{$Left:[0.3,0.7]},$Easing:{$Left:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2}
            ];

            var jssor_1_options = {
              $AutoPlay: 0,
              $Cols: 1,
              $Align: 0,
              $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              },
              $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Cols: 6,
                $Orientation: 2,
                $Align: 156
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 2000;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };

	/*Slider homepage*/
	


	